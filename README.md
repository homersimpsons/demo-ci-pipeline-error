This is a demo project to stress-test GitLab's pipeline page with huge error outputs.

The goal is to have a reproduction for the following issue https://gitlab.com/gitlab-org/gitlab/-/issues/423449.

## How to use

1. Edit the `.gitlab-ci.yml` file to change the number of jobs to run and the length of the error output.
2. Open the pipeline page and notice the server response time.
